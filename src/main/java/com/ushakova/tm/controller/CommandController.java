package com.ushakova.tm.controller;

import com.ushakova.tm.api.ICommandController;
import com.ushakova.tm.api.ICommandService;
import com.ushakova.tm.model.Command;
import com.ushakova.tm.util.NumberUtil;

public class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showAbout() {
        System.out.println("[ABOUT]");
        System.out.println("NAME: Valentina Ushakova");
        System.out.println("E-MAIL: vushakova@tsconsulting.com");
    }

    @Override
    public void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.0.0");
    }

    @Override
    public void showHelp() {
        System.out.println("[HELP]");
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String name = command.getName();
            if(name == null) continue;
            System.out.println(command);
        }
    }

    @Override
    public void showCommands() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) System.out.println(command.getName());
    }

    @Override
    public void showArguments() {
        final Command[] commands = commandService.getTerminalCommands();
        for (final Command command: commands) {
            final String arg = command.getArg();
            if(arg == null) continue;
            System.out.println(arg);
        }
    }

    @Override
    public void showSystemInfo() {
        final int processors = Runtime.getRuntime().availableProcessors();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final String maxMemoryFormat = NumberUtil.formatBytes(maxMemory);
        final String maxMemoryValue = (maxMemory == Long.MAX_VALUE) ? "no limit" : maxMemoryFormat;
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long usedMemory = totalMemory - freeMemory;
        System.out.println("[SYSTEM INFO]");
        System.out.println("[INFO]");
        System.out.println("Available processors: " + processors);
        System.out.println("Free memory: " +  NumberUtil.formatBytes(freeMemory));
        System.out.println("Maximum memory: " + maxMemoryValue);
        System.out.println("Total memory available to JVM: " + NumberUtil.formatBytes(totalMemory));
        System.out.println("Used memory by JVM: " + NumberUtil.formatBytes(usedMemory));
        System.out.println("[OK]");
    }

}
