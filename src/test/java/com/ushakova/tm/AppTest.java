package com.ushakova.tm;

import static org.junit.Assert.assertTrue;

import com.ushakova.tm.bootstrap.Bootstrap;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

public class AppTest {

    @Rule
    public ExpectedSystemExit expectedSystemExit = ExpectedSystemExit.none();

    @Test
    public void showInfo() {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap info = new Bootstrap();
        info.run("-i");
    }

    @Test
    public void showHelp() {
        expectedSystemExit.expectSystemExitWithStatus(0);
        final Bootstrap help = new Bootstrap();
        help.run("-h");
    }

}